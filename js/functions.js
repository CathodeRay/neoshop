/**
 * TODO: Desarrollar el código del slider
 */

$.get("api/slides.json", function(data){  //connection of api

  var currentSlide = 1;                   //number of the current slide
  var speed = 2000;                       //speed of autoPlay
  var pause = 4000;                       //time of pause for each slide 
  var prev = $('.slider__buttonPrev');    //button '<'
  var next = $('.slider__buttonNext');    //button '>'
  var auto;
  
  var numberData = 0;                     //length of data
  var imgWidth = $('.slider__container').width();   //width of screen
  var totalWidth;                         //width of all slide
 
  $.each(data.data,function(i,d){
    $('.slider__ul').append('<li class="slider__li">'+
    '<img src='+d.bg_image+' >'+
    '</li>');
    numberData++;
  });

  $('.slider__ul').append('<li class="slider__li">'+
    '<img src='+data.data[0].bg_image+' >'+
    '</li>');                            //add one more slide in the end in order to link the first one

  totalWidth = imgWidth*(numberData+1);
  $('.slider__ul').css('width', totalWidth);
  $('.slider__li').css({"width":imgWidth,"float":"left"});

  function autoPlay()
  {
     auto=setInterval(function(){
      $('.slider__ul').animate({'margin-left':'-='+imgWidth},speed,function(){
        currentSlide++;
        if(currentSlide >= numberData+1)
        {
          $('.slider__ul').css('margin-left',0);
          currentSlide = 1;
        }
      });
    },pause);
  }

  function stopAutoPlay()
  {
    clearInterval(auto);
  }

  prev.on('click',function(){
    if(currentSlide == 1)
    {
      $('.slider__ul').css('margin-left',-totalWidth+imgWidth);
      currentSlide = numberData+1;
    }
    $('.slider__ul').animate({'margin-left':'+='+imgWidth},speed,function(){currentSlide--;});
  });

  next.click(function(){
    if(currentSlide == numberData+1)
    {
      currentSlide = 1;
      $('.slider__ul').css('margin-left',0);
    }
    $('.slider__ul').animate({'margin-left':'-='+imgWidth},speed,function(){currentSlide++;});
  });

  //stop autoplay when mouse enter the button
  prev.on('mouseenter',stopAutoPlay).on('mouseleave',autoPlay);
  next.on('mouseenter',stopAutoPlay).on('mouseleave',autoPlay);

  autoPlay();
});


/**
 * TODO: Desarrollar el código para cargar productos por Ajax
 */

$.get("api/products.json", function(data){
  
  var count=0; //position of data

  $.each(data.data,function(i,d){

    count++;
    $('.products__container').append('<div class="products__item">'+
    '<div class="products__photo"><img src='+d.image+' ></div>'+                    //place of photo
    '<div class="products__info"><div class="products__name">'+d.name+              //place of name
    '</div><div class="products__price">'+d.price+'</div></div>'+                   //place of price
    '<div class="products__button"><button class="button">'+
    '<a href='+d.button_link+'>'+d.button_text+'</a></button></div></div>');        //place of button
    
    if(count==4) //place of cta1
    {
      $('.products__container').append('<div class="products__cta"><img src="resources/cta/cta1.jpg" ></div>');
    }
    
    if(count==7) //place of cta2
    {
      $('.products__container').append('<div class="products__cta"><img src="resources/cta/cta2.jpg" ></div>');
    }

  });
});

/*
$('.load__button').on('click',function(){
  
});
*/